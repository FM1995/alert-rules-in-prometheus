# Alert Rules in Prometheus Part 1

#### Project Outline
In this project we will configure a monitoring stack to notify us for two scenarios we can use communication channel like an email to get notified. So in this project we will use Alert Manager and the Prometheus stack.

•	When CPU usage is above 50%

•	When a pod cannot start

We will configure this architecture

![Image 1](https://gitlab.com/FM1995/alert-rules-in-prometheus/-/raw/main/Images/Image1.png)

##### Lets get started

Lets open up Prometheus UI

Can run the below to get the service

![Image 2](https://gitlab.com/FM1995/alert-rules-in-prometheus/-/raw/main/Images/Image2.png)

Can see in the service the kube-prometheus service

![Image 3](https://gitlab.com/FM1995/alert-rules-in-prometheus/-/raw/main/Images/Image3.png)

Since it is an internal service, can use the below command

```
kubectl port-forward service/prometheus-monitoring-kube-prometheus -n monitoring 9090:9090
```

![Image 4](https://gitlab.com/FM1995/alert-rules-in-prometheus/-/raw/main/Images/Image4.png)

Can then access the UI

![Image 5](https://gitlab.com/FM1995/alert-rules-in-prometheus/-/raw/main/Images/Image5.png)

Navigating to the alerts page in Prometheus, can create alerts.yaml and use the structure just like the below and then change the values.

![Image 6](https://gitlab.com/FM1995/alert-rules-in-prometheus/-/raw/main/Images/Image6.png)

Can start off with the below metric

```
node_cpu_seconds_total
```

The node_cpu_seconds_total metric in Prometheus is a counter metric from the Node Exporter, which provides detailed information about the CPU usage on a node (a node typically being a machine or VM instance).

This metric counts the total number of CPU seconds consumed by the system, broken down by different modes.

Can see the below for different modes

![Image 7](https://gitlab.com/FM1995/alert-rules-in-prometheus/-/raw/main/Images/Image7.png)

Can then filter on idle, which is the one where it is not being used

![Image 8](https://gitlab.com/FM1995/alert-rules-in-prometheus/-/raw/main/Images/Image8.png)

Can then also do it on the average rate over a period of 2 minutes

![Image 9](https://gitlab.com/FM1995/alert-rules-in-prometheus/-/raw/main/Images/Image9.png)

Can then do the average of the instances 

![Image 10](https://gitlab.com/FM1995/alert-rules-in-prometheus/-/raw/main/Images/Image10.png)

Can then multiply it by 100 to get a percentage to get the results and execute

Looking at the results, can see the nodes are under utilized

![Image 11](https://gitlab.com/FM1995/alert-rules-in-prometheus/-/raw/main/Images/Image11.png)

Can then get the actual usage by subtracting 100 from it

```
100 -(avg by(instance)(rate(node_cpu_seconds_total{mode="idle"}[2m]))* 100)
```

![Image 12](https://gitlab.com/FM1995/alert-rules-in-prometheus/-/raw/main/Images/Image12.png)

Can then add logic to fire when the utilization is greater than 50%

Just like the below

```
100 -(avg by(instance)(rate(node_cpu_seconds_total{mode="idle"}[2m]))* 100) > 50
```

And then placing it in our YAML file

![Image 13](https://gitlab.com/FM1995/alert-rules-in-prometheus/-/raw/main/Images/Image13.png)

Can also add comments and obtain the exact CPU load in the summary

![Image 14](https://gitlab.com/FM1995/alert-rules-in-prometheus/-/raw/main/Images/Image14.png)

Can also modify and include the instance label and label namespace

![Image 15](https://gitlab.com/FM1995/alert-rules-in-prometheus/-/raw/main/Images/Image15.png)

Now that we have the above configured, how do we add it to the list of alert rules already configured in our Prometheus stack.

We can navigate to the following

Status -> Configuration

Can then see the configuration the rules file and that is where the alert rules are defined and they are defined in the rule_files

![Image 16](https://gitlab.com/FM1995/alert-rules-in-prometheus/-/raw/main/Images/Image16.png)


Despite having the file location we don’t need to do file manipulation, since we have Prometheus operator running as a cluster, this lets us create custom Kubernetes components, which defined by CRD’s(custom resource definition) to create alert rules

The Prometheus Operator will then notify Prometheus to update the alert rules

The Prometheus Operator extends the Kubernetes API, which allows to create custom K8S resources, operator then tell custom k8s to reload the alert rules
Modifying the alert-rules.yaml

![Image 17](https://gitlab.com/FM1995/alert-rules-in-prometheus/-/raw/main/Images/Image17.png)

‘monitoring.coreos.com/v1 refers to a specific version (v1) of the API for CoreOS monitoring. This is typically used in the context of Kubernetes and refers to the API group used for setting up and configuring monitoring tools, particularly Prometheus, which is widely used for monitoring Kubernetes clusters.

Can configure the kind and metadata

![Image 18](https://gitlab.com/FM1995/alert-rules-in-prometheus/-/raw/main/Images/Image18.png)

Now to navigate to the Prometheus page to get the spec

https://docs.openshift.com/container-platform/4.10/rest_api/monitoring_apis/prometheus-monitoring-coreos-com-v1.html

![Image 19](https://gitlab.com/FM1995/alert-rules-in-prometheus/-/raw/main/Images/Image19.png)

And the one we are creating is Prometheus rule

![Image 20](https://gitlab.com/FM1995/alert-rules-in-prometheus/-/raw/main/Images/Image20.png)

Below are the specs which can be used in the yaml file

![Image 21](https://gitlab.com/FM1995/alert-rules-in-prometheus/-/raw/main/Images/Image21.png)

We also have the attributes of the group which are required, one of them is name

![Image 22](https://gitlab.com/FM1995/alert-rules-in-prometheus/-/raw/main/Images/Image22.png)

Highlighted is the group name ‘alertmanager.rules’

![Image 23](https://gitlab.com/FM1995/alert-rules-in-prometheus/-/raw/main/Images/Image23.png)

Can define the group name as a string

![Image 24](https://gitlab.com/FM1995/alert-rules-in-prometheus/-/raw/main/Images/Image24.png)

Can then define the rules as an array as seen in the documentation

Can see the alert rule definition

![Image 25](https://gitlab.com/FM1995/alert-rules-in-prometheus/-/raw/main/Images/Image25.png)

Defining the rule alert name

![Image 26](https://gitlab.com/FM1995/alert-rules-in-prometheus/-/raw/main/Images/Image26.png)

Since we have the attributes in the next yaml description can append it

And do it like the below

![Image 27](https://gitlab.com/FM1995/alert-rules-in-prometheus/-/raw/main/Images/Image27.png)

Let’s write another alert for crash looping status

Can use the below metric ‘kube_pod_container_status_restarts_total’

The metric kube_pod_container_status_restarts_total is used in Kubernetes monitoring, specifically with Prometheus. It represents the total number of times all containers in a pod have 
restarted. This metric is useful for identifying and troubleshooting stability issues within a Kubernetes cluster.

![Image 28](https://gitlab.com/FM1995/alert-rules-in-prometheus/-/raw/main/Images/Image28.png)

So if we want to see a container in a pod that keeps on restarting more than 5 times we can then send an alert

Like the below expression

```
kube_pod_container_status_restarts_total > 5
```

and incorporate that in to the alarm

Can then configure it like the below

![Image 29](https://gitlab.com/FM1995/alert-rules-in-prometheus/-/raw/main/Images/Image29.png)

Can then configure the labels and the pod that has this issue

![Image 30](https://gitlab.com/FM1995/alert-rules-in-prometheus/-/raw/main/Images/Image30.png)

Can configure it like the below with pod name and value

![Image 31](https://gitlab.com/FM1995/alert-rules-in-prometheus/-/raw/main/Images/Image31.png)

Next we need to add labels so that the Prometheus operator can pick up the new rules

So according to the below

![Image 32](https://gitlab.com/FM1995/alert-rules-in-prometheus/-/raw/main/Images/Image32.png)

Can configure it like the below

![Image 33](https://gitlab.com/FM1995/alert-rules-in-prometheus/-/raw/main/Images/Image33.png)

Next let’s apply the alert rules

Progress alert rules file

![Image 34](https://gitlab.com/FM1995/alert-rules-in-prometheus/-/raw/main/Images/Image34.png)

Can then run the below command

And we don’t have to specify the namespace as it is

```
kubectl apply -f alert-rules.yaml
```

![Image 35](https://gitlab.com/FM1995/alert-rules-in-prometheus/-/raw/main/Images/Image35.png)

Can then run the below code

```
kubectl get PrometheusRule -n monitoring
```

And see our main rule has been deployed

![Image 36](https://gitlab.com/FM1995/alert-rules-in-prometheus/-/raw/main/Images/Image36.png)

Can see the Alert rules got configured

![Image 37](https://gitlab.com/FM1995/alert-rules-in-prometheus/-/raw/main/Images/Image37.png)

Now if we want to troubleshoot if they don’t appear in the UI, we can check the pods

Can see all the pods

```
kubectl get pod -n monitoring

kubectl describe pod prometheus-prometheus-monitoring-kube-prometheus-0 -n monitoring
```

Can see that the targeted pod has two containers

prometheus-prometheus-monitoring-kube-prometheus-0

![Image 38](https://gitlab.com/FM1995/alert-rules-in-prometheus/-/raw/main/Images/Image38.png)

And can see the names of the containers ‘prometheus’ and ‘config-reloader’

![Image 39](https://gitlab.com/FM1995/alert-rules-in-prometheus/-/raw/main/Images/Image39.png)

Can then run the below for config reloader

```
kubectl logs prometheus-prometheus-monitoring-kube-prometheus-0 -n monitoring -c config-reloader
```

and see it has triggered, giving us assurance that the rules have been implemented

![Image 40](https://gitlab.com/FM1995/alert-rules-in-prometheus/-/raw/main/Images/Image40.png)

Deploying a pod in our cluster that has an application inside that simulates cpu load so that we can initiate a spike and see it in the Grafana dashboards

Can navigate to the docker page and install a CPU stress test using docker image just like the below

![Image 41](https://gitlab.com/FM1995/alert-rules-in-prometheus/-/raw/main/Images/Image41.png)


Below we have an example usage

```
bash docker run -it --name cpu-stress --rm containerstack/cpustress --cpu 4 --timeout 30s --metrics-brief
```

And then converting to a kubetcl command so that we can run it as a pod

```
kubectl run cpu-test --image=containerstack/cpustress -- --cpu 4 --timeout 30s --metrics-brief
```

Below is a quick breakdown of the command

The command kubectl run cpu-test is used to create a pod named cpu-test in a Kubernetes cluster using the containerstack/cpustress image. This image is a tool designed for CPU stress testing. The parameters --cpu 4 --timeout 30s --metrics-brief are arguments for the cpustress tool, instructing it to test 4 CPU cores for a duration of 30 seconds and to provide a brief summary of the test metrics. The double dash -- signifies the end of kubectl options and the beginning of arguments for the container.

Can see the pod when executed

![Image 42](https://gitlab.com/FM1995/alert-rules-in-prometheus/-/raw/main/Images/Image42.png)

Going into the Promethues UI

Can see the below it is pending

![Image 43](https://gitlab.com/FM1995/alert-rules-in-prometheus/-/raw/main/Images/Image43.png)

And can see it is firing

![Image 44](https://gitlab.com/FM1995/alert-rules-in-prometheus/-/raw/main/Images/Image44.png)

And then back to normal due to the timeout

![Image 45](https://gitlab.com/FM1995/alert-rules-in-prometheus/-/raw/main/Images/Image45.png)

Can also see the Grafana dashboard

Before

![Image 46](https://gitlab.com/FM1995/alert-rules-in-prometheus/-/raw/main/Images/Image46.png)

And After

![Image 47](https://gitlab.com/FM1995/alert-rules-in-prometheus/-/raw/main/Images/Image47.png)


















